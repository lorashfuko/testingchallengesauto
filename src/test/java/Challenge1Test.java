import io.qameta.allure.Epic;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.Challenge1Pages;

@DisplayName("Тестирование Challenge1 на http://testingchallenges.thetestingmap.org/")
public class Challenge1Test extends Challenge1Pages {

    @Epic("Тест на Minimum value")
    @Test
    public void challenge1Test1() {
        //Открываем страницу в браузере
        openTestUrl();
        //Устанавливаем значение
        String firstNameField = "I";
        searchField().sendKeys(firstNameField);
        //Отправляем форму
        searchField().submit();
        //Устанавливаем название проверки
        String checksName = "Minimum value";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на Maximum value")
    @Test
    public void challenge1Test2() {
        openTestUrl();
        String firstNameField = "IvanIvanIvanIvanIvanIvanIvanIv";
        searchField().sendKeys(firstNameField);
        searchField().submit();
        String checksName = "Maximum values";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на Average value")
    @Test
    public void challenge1Test3() {
        openTestUrl();
        String firstNameField = "Ivan";
        searchField().sendKeys(firstNameField);
        searchField().submit();
        String checksName = "Average value";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на Space in the middle")
    @Test
    public void challenge1Test4() {
        openTestUrl();
        String firstNameField = "Ivan Petrov";
        searchField().sendKeys(firstNameField);
        searchField().submit();
        String checksName = "Space in the middle";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на Space values at the beginning")
    @Test
    public void challenge1Test5() {
        openTestUrl();
        String firstNameField = " Ivan";
        searchField().sendKeys(firstNameField);
        searchField().submit();
        String checksName = "Space values at the beginning";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на Space values at the end")
    @Test
    public void challenge1Test6() {
        openTestUrl();
        String firstNameField = "Ivan ";
        searchField().sendKeys(firstNameField);
        searchField().submit();
        String checksName = "Space values at the end";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на Other chars then alphabetic")
    @Test
    public void challenge1Test7() {
        openTestUrl();
        String firstNameField = "Ivan12";
        searchField().sendKeys(firstNameField);
        searchField().submit();
        String checksName = "Other chars then alphabetic";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на More than maximum values")
    @Test
    public void challenge1Test8() {
        openTestUrl();
        String firstNameField = "IvanIvanIvanIvanIvanIvanIvanIva";
        searchField().sendKeys(firstNameField);
        searchField().submit();
        String checksName = "More than maximum values";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на Non ASCII")
    @Test
    public void challenge1Test9() {
        openTestUrl();
        String firstNameField = "Иван";
        searchField().sendKeys(firstNameField);
        searchField().submit();
        String checksName = "Non ASCII";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на Basic Sql injection")
    @Test
    public void challenge1Test10() {
        openTestUrl();
        String firstNameField = "admin'or 1=1 or ''='";
        searchField().sendKeys(firstNameField);
        searchField().submit();
        String checksName = "Basic Sql injection";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на Space")
    @Test
    public void challenge1Test11() {
        openTestUrl();
        String firstNameField = " ";
        searchField().sendKeys(firstNameField);
        searchField().submit();
        String checksName = "Space";

        findCheckName(checksName, firstNameField);
    }

    @Epic("Тест на Empty value")
    @Test
    public void challenge1Test12() {
        openTestUrl();
        String firstNameField = "";
        searchField().submit();
        String checksName = "Empty value";

        findCheckName(checksName, firstNameField);
    }
}
