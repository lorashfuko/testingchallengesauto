import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import settings.WebDriverSettings;

public class Challenge2Test extends WebDriverSettings {

    //Определяем переменнную для проверки теста
    Boolean testSuccess = false;

    //Для локатора input`a
    WebElement element;

    public String testUrl = "http://testingchallenges.thetestingmap.org/challenge2.php";

    public void data() {
        driver.get(testUrl);
        //Ищем нужное поля для вставки данных
        element = driver.findElement(By.cssSelector(".six .columns > .inputbox"));
    }

    /**
     * Проверяем числа
     * */
    @Test
    public void challenge2Test1() {
        data();
        //Данные которые будем подставлять
        String data = "15";
        element.sendKeys(data);
        //Данные совпадают?
        if(element.getAttribute("value").equals(data)) {
            //Отправляем
            element.submit();
            //Тест пройден
            testSuccess = true;
        }
        Assertions.assertEquals(testSuccess, true);
    }

    /**
     * Проверяем экспоненциальную запись чисел – 10E0, 1E+6
     * */
    @Test
    public void challenge2Test2() {
        data();
        String data = "1E+6";
        element.sendKeys(data);
        //Данные совпадают?
        if(element.getAttribute("value").equals(data)) {
            element.submit();
            testSuccess = true;
        }
        Assertions.assertEquals(testSuccess, true);
    }

    /**
     * Проверяем не числа (в данном тесте кирилический знак).
     * Данные не должны подставится, следовательно и не должны совпадать.
     * */
    @Test
    public void challenge2Test3() {
        data();
        String data = "Й";
        element.sendKeys(data);
        //Если данные в поле не совпадают идём дальше
        if(!element.getAttribute("value").equals(data)) {
            element.submit();
            testSuccess = true;
        }
        Assertions.assertEquals(testSuccess, true);
    }

}
