import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import settings.WebDriverSettings;

import java.time.YearMonth;
import java.util.Calendar;
import java.util.Random;

/**
 * Условия и тестируемая страница
 * http://testingchallenges.thetestingmap.org/challenge4.php
 *
 * Таблицей расчета контрольного номера
 * https://docs.google.com/spreadsheets/d/1GfZmjTxOVZKjDeoRPFeUfzeOPuIdezzDjFvPNfuOucg/edit#gid=0
 * */

public class Challenge4Test extends WebDriverSettings {

    //Для рендомного определения числа пола
    static String[] genderArr = {"male", "female"};
    static String[] residentsArr = {"foreign", "no", "yes"};

    //С помощью данного числа будет рассчитываться контрольный номер
    static String multiplication = "279146358279";

    //Получаем текущий год, день и месяц.
    static int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    static int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
    static int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

    //Тестируемая страница
    static String urlTestPage = "http://testingchallenges.thetestingmap.org/challenge4.php";

    /**
     * Класс возвращает рандомный день роождения рассчитанный на основе конкретного года и месяца.
     * Если год и месяц текущие, рандомноый день рождения не превышает текущий день в месяце
     * */
    public String randomBirth(int year, int month) {
        String rB;
        int day = 0;

        YearMonth yearMonthObject = YearMonth.of(year, month);
        int daysInMonth = yearMonthObject.lengthOfMonth();

        Random random = new Random();
        day = random.ints(01, daysInMonth + 1).findFirst().getAsInt();
        rB = (day > 0 && day < 10) ? String.format("%02d", day) : Integer.toString(day);

        if (year == currentYear && month == currentMonth + 1) {
            day = random.ints(01, currentDay + 1).findFirst().getAsInt();
            rB = (day > 0 && day < 10) ? String.format("%02d", day) : Integer.toString(day);
        }

        return rB;
    }


    /**
     * Класс возврашает рандомное число (год) от 1800 до 2099 но не больше текущего года
     * */
    public String randomYear() {
        Random random = new Random();
        String year = Integer.toString(random.ints(1800,2100).findFirst().getAsInt());

        if(Integer.parseInt(year) > currentYear) {
            year = Integer.toString(random.ints(1800,currentYear + 1).findFirst().getAsInt());
        }
        return year;
    }

    /**
     * Класс возврашает рандомное число (месяц) от 01 до 12
     * Если год текущий, рандомноый месяц не превышает текущий
     * */
    public String randomMonth(String rYear) {
        String month;
        Random random = new Random();
        int rM = random.ints(01,13).findFirst().getAsInt();
        month = (rM > 0 && rM < 10) ? String.format("%02d", rM) : Integer.toString(rM);

        if(Integer.parseInt(rYear) == currentYear) {
            rM = random.ints(01,currentMonth + 1).findFirst().getAsInt();
            month = (rM > 0 && rM < 10) ? String.format("%02d", rM) : Integer.toString(rM);
        }

        return month;
    }


    /**
     * Класс возврашает рандомное число (код области) от 01 до 52
     * */
    public String randomAreaCode() {
        String area;
        Random random = new Random();
        int a = random.ints(01,53).findFirst().getAsInt();
        area = (a > 0 && a < 10) ? String.format("%02d", a) : Integer.toString(a);
        return area;
    }

    /**
     * Класс возврашает рандомное число (order number) от 1 до 999
     * */
    public String randomOrderNumber() {
        String orderNumber;
        Random random = new Random();
        int n = random.ints(1,1000).findFirst().getAsInt();
        orderNumber = (n > 0 && n < 100) ? String.format("%03d", n) : Integer.toString(n);
        return orderNumber;
    }

    public String genderNumber(int year) {
        //Генерируем случайное значение из массива резидентов и гендера
        Random i = new Random();
        String gender = genderArr[i.nextInt(genderArr.length)];
        String residentsIs = residentsArr[i.nextInt(residentsArr.length)];

        String gN = "";
        if(residentsIs == "yes") {
            if(year >= 1800 && year <= 1899) {
                if(gender == "male") {
                    gN = "3";
                } else if (gender == "female") {
                    gN = "4";
                }
            } else if(year >= 1900 && year <= 1999) {
                if(gender == "male") {
                    gN = "1";
                } else if (gender == "female") {
                    gN = "2";
                }
            } else if(year >= 2000 && year <= 2099) {
                if(gender == "male") {
                    gN = "5";
                } else if (gender == "female") {
                    gN = "6";
                }
            }
        } else if (residentsIs == "foreign") {
            if(gender == "male") {
                gN = "7";
            } else if (gender == "female") {
                gN = "8";
            }
        } else if (residentsIs == "no") {
                gN = "9";
        }
        return gN;
    }

    /**
     * Перемножаем каждый символ полученной строки с установленной
     * Суммируем. Делим на 11 или получаем остаток от деления на 11
     * */
    public String controlNumber(String code) {
        int tResult = 0;
        String cN = "";
        if(code.length() > 0) {
            for(int i = 0; code.length() > i; i++) {
                int t = Integer.parseInt(code.substring(i, code.length() - code.length() + (i + 1)));
                int t2 = Integer.parseInt(multiplication.substring(i, multiplication.length() - multiplication.length() + (i + 1)));
                tResult += t*t2;
            }

            if(tResult / 11 == 10 || tResult % 11 == 10) {
                cN = "1";
            } else {
                cN = Integer.toString(tResult % 11);
            }
        }
        return cN;
    }

    public String idNumber() {

        String rYear = randomYear();
        String rMonth = randomMonth(rYear);
        String gender = genderNumber(Integer.parseInt(rYear));
        String year = rYear.substring(rYear.length() -2);//последние два числа года
        String month = rMonth;
        String birth = randomBirth(Integer.parseInt(rYear), Integer.parseInt(rMonth));
        String areaCode = randomAreaCode();
        String orderNumber = randomOrderNumber();

        String code = gender + year + month + birth + areaCode + orderNumber;

        return code;
    }

    /**
     * Генерирует 5 рандомных номеров и проверяет их корректность
     * TODO: Подумай как сделать проверку если рандомное значение повторится в момент проверки. Если это происходит система отдает alert "You have used this CNP before"
     * */
    @Test
    public void Challenge4Test1() {
        driver.get(urlTestPage);
        String idN = "";
        int j = 5;
        for (int i = 0; i <= j; i++) {
            if(i < j) {
                idN = idNumber();
                String cnp = idN + controlNumber(idN);
                WebElement searchField = driver.findElement(By.cssSelector("input[name=CNP]"));
                searchField.sendKeys(cnp);
                searchField.submit();
            } else {
                Assertions.assertTrue(true);
            }
        }
    }

    /**
     * Проверяет наличие алерта если число месяца больше календарного
     * */
    @Test
    public void Challenge4Test2() {
        driver.get(urlTestPage);
        String cnp = "9811325262893";
        WebElement searchField = driver.findElement(By.cssSelector("input[name=CNP]"));
        searchField.sendKeys(cnp);
        searchField.submit();
        Assertions.assertEquals(driver.switchTo().alert().getText(),"Invalid Month");
    }

    /**
     * Проверяет наличие алерта если число дня рождения больше календарного
     * */
    @Test
    public void Challenge4Test3() {
        driver.get(urlTestPage);
        String cnp = "9810132262892";
        WebElement searchField = driver.findElement(By.cssSelector("input[name=CNP]"));
        searchField.sendKeys(cnp);
        searchField.submit();
        Assertions.assertEquals(driver.switchTo().alert().getText(),"Invalid Day");
    }

    /**
     * Проверяет длинну строки. В тесте меньше на один символ (12 символов)
     * */
    @Test
    public void Challenge4Test4_1() {
        driver.get(urlTestPage);
        String cnp = "981013226289";
        WebElement searchField = driver.findElement(By.cssSelector("input[name=CNP]"));
        searchField.sendKeys(cnp);
        searchField.submit();
        Assertions.assertEquals(driver.switchTo().alert().getText(),"Invalid length or content");
    }

    /**
     * Проверяет длинну строки. В тесте больше на один символ (14 символов)
     * */
    @Test
    public void Challenge4Test4_2() {
        driver.get(urlTestPage);
        String cnp = "98101322628912";
        WebElement searchField = driver.findElement(By.cssSelector("input[name=CNP]"));
        searchField.sendKeys(cnp);
        searchField.submit();
        Assertions.assertEquals(driver.switchTo().alert().getText(),"Invalid length or content");
    }

    /**
     * Проверяет, что при установленной длинне строки значение не соответствует условию
     * */
    @Test
    public void Challenge4Test4_3() {
        driver.get(urlTestPage);
        String cnp = "qwertyuiopdlk";
        WebElement searchField = driver.findElement(By.cssSelector("input[name=CNP]"));
        searchField.sendKeys(cnp);
        searchField.submit();
        Assertions.assertEquals(driver.switchTo().alert().getText(),"Invalid length or content");
    }

    /**
     * Проверяет два одинаковых значения подряд
     * */
    @Test
    public void Challenge4Test5() {
        driver.get(urlTestPage);
        String cnp = "1110105019016";

        WebElement searchField = driver.findElement(By.cssSelector("input[name=CNP]"));

        searchField.sendKeys(cnp);
        searchField.submit();

        searchField = driver.findElement(By.cssSelector("input[name=CNP]"));
        searchField.sendKeys(cnp);
        searchField.submit();

        Assertions.assertEquals(driver.switchTo().alert().getText(),"You have used this CNP before");
    }

}
