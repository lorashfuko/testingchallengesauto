package pages;

import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import settings.WebDriverSettings;

import java.util.List;

public class Challenge1Pages extends WebDriverSettings {

    //Определяем переменнную для проверки теста
    public Boolean testSuccess = false;
    //Локатор в котором должны выводится названия проверок
    public String locatorItemsChecks = "//*[@class='values-description t10']//li";
    //Тестируемая страница
    public String urlTestPage = "http://testingchallenges.thetestingmap.org/index.php";
    //ID поля в которое будут подставлятся значения
    public String idFieldFirstName = "firstname";

    //Выбираем все названия проверок если их много
    public List<WebElement> listChecks() {
        List<WebElement> allChecks = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(locatorItemsChecks)));
        return allChecks;
    }

    public void findCheckName(String cName, String firstNameField) {
        List<WebElement> allChecks = listChecks();
        String fCName = "";
        //Если элементы появились (!allChecks.isEmpty()) на странице, идём дальше
        if (!allChecks.isEmpty()) {
            for (WebElement checkName : allChecks) {
                //Нужное название проверки найдено – тест пройден
                if (checkName.getText().equals(cName)) {
                    fCName = checkName.getText();
                    testSuccess = true;
                }
            }
            testResult(cName, fCName, firstNameField);
        }
    }


    // TODO: 31.03.2021 Нужно разобраться на сколько корректно использовать аннотацию @BeforeEach больше одного раза и в разных файлах.
    @BeforeEach
    public void waiting() {
        //Отсрочка до появления списка проверок
        wait = new WebDriverWait(driver, 5);
    }

    @Step("Данные: cName == fCName => ok")
    public void testResult(String cName, String fCName, String firstNameFieldData) {
        Assertions.assertEquals(testSuccess, true);
    }

    //Ищем нужное поле
    public WebElement searchField() {
        WebElement searchField = driver.findElement(By.id(idFieldFirstName));
        return searchField;
    }

    //открываем страницу
    public void openTestUrl() {
        driver.get(urlTestPage);
    }
}
