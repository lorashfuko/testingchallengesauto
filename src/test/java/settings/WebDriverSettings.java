package settings;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverSettings {

    public WebDriver driver;
    public WebDriverWait wait;

    /**
     * Код который выполнится до теста
     * */
    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/Users/lorashfuko/Documents/browserdriver/chrome/chromedriver");

        //Не открываем браузер при прохождении тестов
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

        driver = new ChromeDriver(options);

    }

    /**
     * Код который выполнится после теста
     * */
    @AfterEach
    public void close() {
        driver.quit();
    }

}
